package com.task55s10.pizzaapi;

import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class Controller {
    CDailyCampaign myMessage = new CDailyCampaign();

    @GetMapping("message")
    public String getMesage() {
        java.util.Date date = new java.util.Date();
        String dayOfWeek = new SimpleDateFormat("EEEE").format(date);
        return dayOfWeek;
    }
}