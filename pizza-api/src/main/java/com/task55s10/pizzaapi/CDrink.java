package com.task55s10.pizzaapi;

import java.util.Date;

public class CDrink {
    private String maNuocUong = "";
    private String tenNuocUong = "";
    private int donGia = 0;
    private String ghiChu = "";
    private long ngayTao;
    private long ngayCapNhat;
    
    public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, long ngayTao, long ngayCapNhat) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public String toString() {
        return "CDrink [donGia=" + donGia + ", ghiChu=" + ghiChu + ", maNuocUong=" + maNuocUong + ", ngayCapNhat="
                + ngayCapNhat + ", ngayTao=" + ngayTao + ", tenNuocUong=" + tenNuocUong + "]";
    }
    
    
    
}
