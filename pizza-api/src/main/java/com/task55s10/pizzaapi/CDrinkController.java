package com.task55s10.pizzaapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CDrinkController {
    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrink() {
        ArrayList<CDrink> myDrinkList = new ArrayList<CDrink>();
        CDrink soda = new CDrink("SOD", "soda", 1000, "KO BÁN CHỊU", 26072022, 28072022);
        CDrink suaDam = new CDrink("MILK", "Sữa dằm", 25000, "KO BÁN CHỊU", 26072022, 28072022);
        CDrink lemon = new CDrink("LEMO", "Lemon", 20000, "KO BÁN CHỊU", 26072022, 28072022);
        CDrink number1 = new CDrink("NUMB", "Number1", 15000, "KO BÁN CHỊU", 26072022, 28072022);
        
        myDrinkList.add(soda);
        myDrinkList.add(suaDam);
        myDrinkList.add(lemon);
        myDrinkList.add(number1);
        
        return myDrinkList;
    }
}
