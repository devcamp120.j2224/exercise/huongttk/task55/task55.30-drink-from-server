package com.task55s10.pizzaapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CComboController {
    @GetMapping("combo")
    public ArrayList<CComboMenu> getCombo() {
        CComboMenu myComboMenuS = new CComboMenu("S", 20, 2, 200, 2, 150000);
        CComboMenu myComboMenuM = new CComboMenu("M", 25, 4, 300, 3, 200000);
        CComboMenu myComboMenuL = new CComboMenu("L", 30, 8, 500, 4, 250000);
        ArrayList<CComboMenu> mArrayListComboMenu = new ArrayList<CComboMenu>();
        mArrayListComboMenu.add(myComboMenuS);
        mArrayListComboMenu.add(myComboMenuL);
        mArrayListComboMenu.add(myComboMenuM);
        return mArrayListComboMenu;
    }
}
